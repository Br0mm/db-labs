CREATE TABLE country (
	id SERIAL NOT NULL PRIMARY KEY,
	name VARCHAR(255) NOT NULL
);

CREATE TABLE food_types (
	id SERIAL NOT NULL PRIMARY KEY,
	type VARCHAR(255) NOT NULL
);

CREATE TABLE serving (
	serving_id INTEGER NOT NULL REFERENCES food_types(id),
	cutlery VARCHAR(255) NOT NULL,
	size VARCHAR(6) NOT NULL
);

CREATE TABLE cooking_types (
	id SERIAL NOT NULL PRIMARY KEY,
	type VARCHAR(255) NOT NULL,
	temperature INTEGER,
	time TIMESTAMP NOT NULL
);

CREATE TABLE receipts (
	id SERIAL NOT NULL PRIMARY KEY,
	receipt_name VARCHAR(255) NOT NULL,
	author VARCHAR(255),
	country_id INTEGER NOT NULL REFERENCES country(id),
	food_type_id INTEGER NOT NULL REFERENCES food_types(id),
	cooking_type_id INTEGER NOT NULL REFERENCES cooking_types(id)
);

CREATE TABLE ingredients_receipts (
	receipt_id INTEGER NOT NULL REFERENCES receipts(id),
	ingredient_name VARCHAR(255) NOT NULL,
	amount INTEGER NOT NULL,
	unit VARCHAR(255) NOT NULL
);

CREATE TABLE "user" (
	id SERIAL NOT NULL PRIMARY KEY,
	user_name VARCHAR(255) NOT NULL,
	user_phone_number VARCHAR(12) NOT NULL
);

CREATE TABLE favourite_receipts (
	user_id INTEGER NOT NULL REFERENCES "user"(id),
	receipt_id INTEGER NOT NULL REFERENCES receipts(id)
);
